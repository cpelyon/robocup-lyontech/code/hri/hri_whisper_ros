# hri_whisper_ros

[TOC]


## Contributors

- T. Pannethier


## Dependencies

```bash
python3 -m pip install -r requirements.txt
```
 
## How to install 

### On linux : 

```bash
git clone git@gitlab.com:cpelyon/robocup-lyontech/code/hri/hri_whisper_ros.git
git submodule update --init --recursive
pip3 install -r audio_common/requirements.txt
pip3 install -r whisper_ros/requirements.txt
cd ~/ros2_ws
colcon build
```

### CUDA

To run llama_ros with CUDA, you have to install the [CUDA Toolkit](https://developer.nvidia.com/cuda-toolkit) and the following line in the [CMakeLists.txt](whisper_ros/whisper_ros/CMakeLists.txt) must be uncommented:

```
option(WHISPER_CUBLAS "whisper: support for cuBLAS" ON)
```

```bash
sudo apt install portaudio19-dev -y
python3 -m pip install -r requirements.txt
```

If the script crash because of **No espeak backend found. Install espeak-ng or espeak to your system**, install espeak-ng : 


```bash
sudo apt-get update
sudo apt-get install espeak-ng
```
### pytorch on Jetson : 

- Uninstall torch :
```bash
pip uninstall torch torchvision torchaudio
```

- Download torch for jetson - jetpack 6.0 :

link : https://forums.developer.nvidia.com/t/pytorch-for-jetson/72048  
package : https://developer.download.nvidia.cn/compute/redist/jp/v60dp/pytorch/torch-2.2.0a0+6a974be.nv23.11-cp310-cp310-linux_aarch64.whl

- Install the package : 
```bash
cd ~/Downloads
pip install torch-2.2.0a0+6a974be.nv23.11-cp310-cp310-linux_aarch64.whl
cd
```

- Install torchaudio for jetson :

link to the pytorch tutorial : https://pytorch.org/audio/stable/build.jetson.html
```bash
pip install cmake ninja

sudo apt install ffmpeg libavformat-dev libavcodec-dev libavutil-dev libavdevice-dev libavfilter-dev

git clone https://github.com/pytorch/audio
cd audio
USE_CUDA=1 pip install -v -e . --no-use-pep517
```
- Check the installation : 

```python
import torchaudio

print(torchaudio.__version__)

torchaudio.utils.ffmpeg_utils.get_build_config()
```



### Open question : 

## How to start


Run Silero for VAD and Whisper for STT:

```shell
ros2 launch whisper_bringup whisper.launch.py
```

Send a goal action to listen:

```shell
ros2 action send_goal /whisper/listen whisper_msgs/action/STT "{}"
```

Or try the example of a whisper client:

```shell
ros2 run whisper_ros whisper_client_node
```
## API

### Topics  
/whisper/text [std_msgs/msg/String]  
/whisper/vad [std_msgs/msg/Float32MultiArray]  

 
### Services 

/whisper/enable_vad [std_srvs/srv/SetBool]  
/whisper/reset_grammar [std_srvs/srv/Empty]  
/whisper/set_grammar [whisper_msgs/srv/SetGrammar]  
/whisper/set_init_prompt [whisper_msgs/srv/SetInitPrompt]  
/whisper/silero_vad_node/describe_parameters [rcl_interfaces/srv/DescribeParameters]  
/whisper/silero_vad_node/get_parameter_types [rcl_interfaces/srv/GetParameterTypes]  
/whisper/silero_vad_node/get_parameters [rcl_interfaces/srv/GetParameters]  
/whisper/silero_vad_node/list_parameters [rcl_interfaces/srv/ListParameters]  
/whisper/silero_vad_node/set_parameters [rcl_interfaces/srv/SetParameters]  
/whisper/silero_vad_node/set_parameters_atomically [rcl_interfaces/srv/SetParametersAtomically]  
/whisper/whisper_manager_node/describe_parameters [rcl_interfaces/srv/DescribeParameters]  
/whisper/whisper_manager_node/get_parameter_types [rcl_interfaces/srv/GetParameterTypes]  
/whisper/whisper_manager_node/get_parameters [rcl_interfaces/srv/GetParameters]  
/whisper/whisper_manager_node/list_parameters [rcl_interfaces/srv/ListParameters]  
/whisper/whisper_manager_node/set_parameters [rcl_interfaces/srv/SetParameters]  
/whisper/whisper_manager_node/set_parameters_atomically [rcl_interfaces/srv/SetParametersAtomically]  
/whisper/whisper_node/describe_parameters [rcl_interfaces/srv/DescribeParameters]  
/whisper/whisper_node/get_parameter_types [rcl_interfaces/srv/GetParameterTypes]  
/whisper/whisper_node/get_parameters [rcl_interfaces/srv/GetParameters]  
/whisper/whisper_node/list_parameters [rcl_interfaces/srv/ListParameters]  
/whisper/whisper_node/set_parameters [rcl_interfaces/srv/SetParameters]  
/whisper/whisper_node/set_parameters_atomically [rcl_interfaces/srv/SetParametersAtomically]  

### Actions  
/whisper/listen [whisper_msgs/action/STT]  

### Messages
